<?php

namespace Drupal\commerce_ef\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Component\Datetime\TimeInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Provides the eFawateercom offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "ef_redirect_checkout",
 *   label = @Translation("eFawateercom (Redirect to eFawateercom website)"),
 *   display_label = @Translation("eFawateercom"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_ef\PluginForm\EfPaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class RedirectCheckout extends OffsitePaymentGatewayBase {

  use LoggerChannelTrait;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new OffsiteRedirect2C2P object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, ClientInterface $http_client, MinorUnitsConverterInterface $minor_units_converter = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);

    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('http_client'),
      $container->get('commerce_price.minor_units_converter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'biller_code' => '',
      'secret_token' => '',
      'payment_type' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['biller_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Biller code'),
      '#description' => $this->t('The biller code that provided by eFawateercom.'),
      '#default_value' => $this->configuration['biller_code'],
      '#required' => TRUE,
    ];

    $form['secret_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret token'),
      '#description' => $this->t('This is the secret token prvided by eFawateercom.'),
      '#default_value' => $this->configuration['secret_token'],
      '#required' => TRUE,
    ];

    $form['bearer_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bearer key'),
      '#description' => $this->t('Bearer authorization key to be used in the onNotify method.'),
      '#default_value' => $this->configuration['bearer_key'],
      '#required' => FALSE,
    ];

    $form['payment_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Payment type'),
      '#description' => $this->t('The payment type code of the biller service.'),
      '#options' => [
        1 => $this->t('Postpaid'),
        2 => $this->t('Prepaid'),
      ],
      '#default_value' => (int) $this->configuration['payment_type'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['biller_code'] = $values['biller_code'];
    $this->configuration['secret_token'] = $values['secret_token'];
    $this->configuration['payment_type'] = $values['payment_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);

    // Log the response message.
    $this->getLogger('commerce_ef')
      ->debug('eFawateercom middleware database notification: <pre>@body</pre>', [
        '@body' => var_export($request->query->all(), TRUE),
      ]);

    $authorization = $request->headers->get('Authorization');
    $bearer_auth_key = 'Bearer ' . $this->configuration['bearer_key'];
    if (empty($authorization) || $authorization != $bearer_auth_key) {
      return new Response('Access token is missing or invalid.', 401);
    }

    // Response processing for the notification.
    $transaction_id = $request->query->get('BilrTrxNo');
    if (!empty($transaction_id)) {
      // Extract the payment id from the transaction id.
      $payment_id = substr($transaction_id, 4);
      $payment = $this->entityTypeManager->getStorage('commerce_payment')->load($payment_id);
      // Check if the payment object is loaded successfully.
      if ($payment instanceof PaymentInterface) {
        // Update the payment state from the biller middleware database.
        $new_state = $request->query->get('PaymentStatus') == 'completed' ? 'completed' : 'voided';
        $payment->set('state', $new_state);
        $remote_trans_id = !empty($request->query->get('DirectPayTrxNo')) ? $request->query->get('DirectPayTrxNo') : '';
        $payment->set('remote_id', $transaction_id);
        $payment->save();
        $order = $payment->getOrder();
        $current_state = $order->getState()->getId();
        if ($current_state == 'validation') {
          if ($new_state == 'completed') {
            // Set the order state to completed by applying the
            // 'validate' transition.
            $order->getState()->applyTransitionById('validate');
          }
          else {
            // Set the order state to canceled by applying the
            // 'cancel' transition.
            $order->getState()->applyTransitionById('cancel');
          }
          $order->save();
          return new Response($new_state, 200);
        }
        else {
          return new Response('The order is already validated.', 200);
        }
      }
    }
    return new Response('Transaction id is missing or invalid.', 500);
  }

}
