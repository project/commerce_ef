<?php

namespace Drupal\commerce_ef\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Url;

/**
 * {@inheritdoc}
 */
class EfPaymentForm extends BasePaymentOffsiteForm {

  /**
   * The payment gateway live environment URL.
   *
   * @var string
   */
  const PAYMENT_LIVE_URL = 'https://www.efawateercom.jo/Portal/DirectPayService/DirectPay.aspx';

  /**
   * The payment gateway test environment URL.
   *
   * @var string
   */
  const PAYMENT_TEST_URL = 'https://staging.efawateercom.jo/DirectPayService/DirectPay.aspx';

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    // Save the payment entity so that we can get its ID and use it for
    // building the 'transaction_id' property for eFawateercom.
    // Then, when user returns from the off-site redirect, we will update
    // the same payment.
    $payment->save();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();
    $order_items = $order->getItems();
    $product_variation_id = $order_items[0]->get('purchased_entity')->getString();
    $product_variation = ProductVariation::load($product_variation_id);
    $product = $product_variation->getProduct();
    $service_code = $product->get('field_service_code')->getString();
    $prepaid_cat_code = $product_variation->get('field_prepaid_cat_code')->getString();
    $price = $product_variation->getPrice()->getNumber();

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();
    $secret_token = $configuration['secret_token'];
    $data['secret_token'] = $secret_token;
    // Add the payment id to the transaction id to help in loading
    // the payment in onnotify.
    $transaction_id = rand(1000, 9999) . $payment->id();
    // Set the remote_id in early stages to be used in tracing the pending
    // orders, the transacion id is the most used so it's used as remote_id.
    $payment->set('remote_id', $transaction_id);
    $payment->save();

    // Set the order state to validation by applying the
    // 'place' transition.
    $order->getState()->applyTransitionById('place');
    $order->set('field_checkout_language', $language);
    $order->save();

    // Prepare eFawateercom payment gateway parameters.
    $data['BilrTrxNo'] = $transaction_id;
    $data['BillerCode'] = $configuration['biller_code'];
    $data['ServiceCode'] = $service_code;
    $data['PaymentType'] = $configuration['payment_type'];
    $data['Currency'] = 'JOD';
    $data['BillingNo'] = $transaction_id;
    $data['PrepaidCatCode'] = $prepaid_cat_code;
    $data['Amount'] = number_format($price, 2);
    $data['StatmntNartive'] = '';
    // From the contact details.
    $data['CustEmail'] = '';
    $data['Language'] = strtoupper(\Drupal::languageManager()->getCurrentLanguage()->getId());
    $data['OtherDetails'] = '';
    $payment_redirect_url = $configuration['mode'] == 'test' ? self::PAYMENT_TEST_URL : self::PAYMENT_LIVE_URL;

    return $this->buildRedirectForm(
      $form,
      $form_state,
      $payment_redirect_url,
      $data,
      self::REDIRECT_GET
    );
  }

  /**
   * Builds the redirect form.
   *
   * @param array $form
   *   The plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $redirect_url
   *   The redirect url.
   * @param array $data
   *   Data that should be sent along.
   * @param string $redirect_method
   *   The redirect method (REDIRECT_GET or REDIRECT_POST constant).
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   *   The redirect exception, if $redirect_method is REDIRECT_GET.
   */
  protected function buildRedirectForm(array $form, FormStateInterface $form_state, $redirect_url, array $data, $redirect_method = self::REDIRECT_GET) {
    if ($redirect_method == self::REDIRECT_GET) {
      $secret_token = $data['secret_token'];
      unset($data['secret_token']);
      $params = implode('|', $data);
      $hash = password_hash($params . '|' . $secret_token, PASSWORD_BCRYPT, ['cost' => 10]);
      // Workaround for fixing 2y prefixes
      // https://stackoverflow.com/questions/15733196/where-2x-prefix-are-used-in-bcrypt
      $hash = '$2a$' . substr($hash, 4);
      $params = "{$params}|{$hash}";
      $redirect_url = Url::fromUri($redirect_url, ['query' => ['RequestParams' => $params]])->toString();
      \Drupal::logger('commerce_ef')
        ->debug('eFawateercom request URL: @url <pre>@body</pre>', [
          '@url' => $redirect_url,
          '@body' => var_export($params, TRUE),
        ]);
      throw new NeedsRedirectException($redirect_url);
    }
  }

}
