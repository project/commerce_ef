<?php

namespace Drupal\commerce_ef\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Handles eFawateercom payment redirect by reading the query string.
 *
 * Then redirect the user to commerce on return route.
 */
class PaymentReturnController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function return() {
    $build = [
      '#markup' => $this->t('Payment redirect controller.'),
    ];
    return $build;
  }

}
