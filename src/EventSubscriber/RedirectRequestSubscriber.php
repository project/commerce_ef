<?php

namespace Drupal\commerce_ef\EventSubscriber;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_ef\LanguageCookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RequestContext;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\redirect\Exception\RedirectLoopException;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Redirect subscriber for controller requests.
 */
class RedirectRequestSubscriber implements EventSubscriberInterface {
  use StringTranslationTrait;

  /**
   * Common interface for the language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The default alias manager implementation.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Interface for classes that manage a set of enabled modules.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Holds information about the current request.
   *
   * @var \Symfony\Component\Routing\RequestContext
   */
  protected $context;

  /**
   * A path processor manager for resolving the system path.
   *
   * @var \Drupal\Core\PathProcessor\InboundPathProcessorInterface
   */
  protected $pathProcessor;

  /**
   * Kill Switch for page caching.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * Logger Channel Factory interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The cookie as a service.
   *
   * @var \Drupal\commerce_ef\LanguageCookie
   */
  protected $languageCookie;

  /**
   * Constructs a RedirectRequestSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The alias manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\Routing\RequestContext $context
   *   Request context.
   * @param \Drupal\Core\PathProcessor\InboundPathProcessorInterface $path_processor
   *   A path processor manager for resolving the system path.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   A Kill Switch for page caching.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A Logger Channel Factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation.
   * @param \Drupal\commerce_ef\LanguageCookie $languageCookie
   *   The cookie as a service.
   */
  public function __construct(LanguageManagerInterface $language_manager, AliasManagerInterface $alias_manager, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, RequestContext $context, InboundPathProcessorInterface $path_processor, KillSwitch $kill_switch, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger, TranslationInterface $string_translation, LanguageCookie $languageCookie) {
    $this->languageManager = $language_manager;
    $this->aliasManager = $alias_manager;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->context = $context;
    $this->pathProcessor = $path_processor;
    $this->killSwitch = $kill_switch;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->LanguageCookie = $languageCookie;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // This needs to run before RouterListener::onKernelRequest(), which has
    // a priority of 32. Otherwise, that aborts the request if no matching
    // route is found.
    $events[KernelEvents::REQUEST][] = ['onKernelRequestCheckRedirect', 256];
    return $events;
  }

  /**
   * Handles the redirect if any found.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   */
  public function onKernelRequestCheckRedirect(RequestEvent $event) {
    // Get a clone of the request. During inbound processing the request
    // can be altered. Allowing this here can lead to unexpected behavior.
    // For example the path_processor.files inbound processor provided by
    // the system module alters both the path and the request; only the
    // changes to the request will be propagated, while the change to the
    // path will be lost.
    $response = new Response();
    $request = clone $event->getRequest();

    try {
      if (strpos($request->getPathInfo(), '/sites/default/files/') === 0 || strpos($request->getPathInfo(), '/themes/') === 0) {
        return;
      }
      $path = $request->getPathInfo();
      $redirect_from = '/complete-direct-payment';
      $redirect_to = '';
      $redirect_language = '';
      $langcode = $this->languageManager->getCurrentLanguage()->getId();

      // The request event can fire more than once. Don't do anything if it's
      // not the master request.
      if ($event->isMasterRequest()) {
        $this->LanguageCookie->setCookieValue($langcode);
      }
      if ($path != $redirect_from) {
        return;
      }

      // Disable caching for this page.
      $this->killSwitch->trigger();
      // Get the query string.
      $query_arr = $request->query->all();
      // // Get the current user language.
      $language_cookie = $request->cookies->get('language_cookie');
      // We don't have /en language prefix we have to exclude the en language.
      // TO DO en should be changed to the site default language.
      if (isset($language_cookie) && $language_cookie != 'en') {
        $redirect_language = '/' . $language_cookie;
      }
      // Explode the ResponseParams parameter to get the response parameters.
      if (isset($query_arr['ResponseParams'])) {
        $response_params = $query_arr['ResponseParams'];
        $this->loggerFactory->get('commerce_ef')->debug('ResponseParams from eFawateecom is %p.', ['%p' => $response_params]);
        $response_params_arr = explode('|', $response_params);
        $transaction_id = $response_params_arr[0];
        $payment_id = substr($transaction_id, 4);
        $payment = $this->entityTypeManager->getStorage('commerce_payment')->load($payment_id);
        // Check if the payment object is loaded successfully.
        if ($payment instanceof PaymentInterface) {
          // Get the order id to redirect the user to the commerce return page.
          $order_id = $payment->getOrderId();
          $payment->set('remote_id', $response_params_arr[2]);
          switch ($response_params_arr[4]) {
            case 1:
              $remote_state = 'completed';
              break;

            case 2:
              $remote_state = 'pending';
              break;

            case 3:
              $remote_state = 'failed';
              break;

            default:
              $remote_state = '';
          }
          $payment->set('remote_state', $remote_state);
          if ($remote_state == 'failed') {
            $payment->set('state', 'failed');
            // The payment failed initially so load the campaign node id to
            // redirect the user to the campaign page.
            $order = $payment->getOrder();
            $order_items = $order->getItems();
            $variation_id = $order_items[0]->get('purchased_entity')->getString();
            $variation = ProductVariation::load($variation_id);
            $product_id = $variation->product_id->getString();
            if ($order->getState()->getId() == 'validation') {
              // Set the order state to canceled by applying the
              // 'cancel' transition.
              $order->getState()->applyTransitionById('cancel');
              $order->save();
            }
            if (isset($product_id)) {
              // Retrieve node related with product.
              $node = $this->entityTypeManager->getStorage('node')->loadByProperties(['field_product' => $product_id]);
              if (isset($node)) {
                $node_id = reset($node)->get('nid')->getString();
                $redirect_to = $redirect_language . '/node/' . $node_id;
                if ($redirect_language == '/ar') {
                  $this->messenger->addMessage($this->t('فشل في عملية الدفع على خادم الدفع. يرجى مراجعة المعلومات الخاصة بك والمحاولة مرة أخرى.'), 'error');
                }
                else {
                  $this->messenger->addMessage($this->t('The payment failed at the payment server. Please review your information and try again.'), 'error');
                }
              }
            }
          }
          else {
            $redirect_to = $redirect_language . '/checkout/' . $order_id . '/payment/return';
          }
          $payment->save();
        }
      }

      if ($redirect_to != '') {
        $url = $redirect_to;
      }
      else {
        $url = '/';
      }
      $this->loggerFactory->get('commerce_ef')->debug('url to redirect to is %r.', ['%r' => $url]);
    }
    catch (RedirectLoopException $e) {
      $path_rid = [
        '%path' => $e->getPath(),
        '%rid' => $e->getRedirectId(),
      ];
      $this->loggerFactory->get('commerce_ef')->warning('Redirect loop identified at %path for redirect %rid', $path_rid);
      $response->setStatusCode(503);
      $response->setContent('Service unavailable');
      $event->setResponse($response);
      return;
    }

    if (!empty($url)) {
      $response = new TrustedRedirectResponse($url, 302);
      $build = [
        '#cache' => [
          'max-age' => 0,
        ],
      ];
      $cache_metadata = CacheableMetadata::createFromRenderArray($build);
      $response->addCacheableDependency($cache_metadata);
      $event->setResponse($response);
    }
  }

}
